module github.com/mdouchement/ldt

go 1.16

require (
	github.com/Shopify/go-lua v0.0.0-20220120202609-9ab779377807
	github.com/Shopify/goluago v0.0.0-20210621135517-fe0528d0b204
	github.com/VividCortex/ewma v1.2.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/muesli/coral v1.0.0
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/vbauerster/mpb/v4 v4.12.2
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
